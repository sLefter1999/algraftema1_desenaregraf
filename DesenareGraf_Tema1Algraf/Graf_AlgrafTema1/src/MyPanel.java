import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.Vector;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel {

	private int nodeNr=0;
	private int nodeDiam=30;
	private Vector<Node> listaNoduri;
	private Vector<Arc> listaArce;
	Point pointStart = null;
	Point pointEnd = null;
	boolean isDragging = false;
	public MyPanel()
	{
		listaNoduri = new Vector<Node>();
		listaArce = new Vector<Arc>();
		setBorder(BorderFactory.createLineBorder(Color.black));
		addMouseListener(new MouseAdapter() {
			//evenimentul care se produce la apasarea mousse-ului
			public void mousePressed(MouseEvent e) {
				pointStart = e.getPoint();
				}
			
			//evenimentul care se produce la eliberarea mousse-ului
			public void mouseReleased(MouseEvent e) {
				if(!isDragging)
					addNode(pointStart);
				else {
					Arc arc=new Arc(pointStart,pointEnd);
					Node startNode=arc.StartNode(listaNoduri)
;					Node endNode=arc.EndNode(listaNoduri);
					
					if(endNode!=null && startNode!=null)
					{
						arc.SetEndNode(endNode);
						arc.SetStartNode(startNode);
						addArc(arc);
					}
				}
				pointStart=null;
				pointEnd=null;
				isDragging=false;
				repaint();
			}
		});
	addMouseMotionListener(new MouseMotionAdapter() {
		//evenimentul care se produce la drag&drop pe mousse
		public void mouseDragged(MouseEvent e) {
			pointEnd = e.getPoint();
			isDragging = true;
			repaint();
		}
	});
	}
		private void addNode(Point point) {
			Node node = new Node(point, nodeNr);
			if(node.DoesNotIntersecWithNodes(listaNoduri))
			{
				listaNoduri.add(node);
				nodeNr++;
				repaint();
			}
		}
		public void addArc(Arc arc)
		{
			if(!ExistaArc(arc))
			listaArce.add(arc);
		}
		
		public boolean ExistaArc(Arc arc)
		{
			for(Arc a:listaArce)
			{
				Node startNode=a.StartNode(listaNoduri);
				Node endNode=a.EndNode(listaNoduri);
					if((startNode==arc.m_startNode && endNode==arc.m_endNode) || (startNode==arc.m_endNode && endNode==arc.m_startNode))
						return true;
			}
			return false;
		}
		protected void paintComponent(Graphics graphic)
		{
			super.paintComponent(graphic);
			
			if (isDragging)
			{
						graphic.setColor(Color.RED);
						graphic.drawLine(pointStart.x, pointStart.y, pointEnd.x, pointEnd.y);
			}
			
			for(Arc arc:listaArce)
				arc.DrawArc(graphic);
			
			for(Node nod:listaNoduri)
				nod.drawNode(graphic,nodeDiam);
		}
	
}
