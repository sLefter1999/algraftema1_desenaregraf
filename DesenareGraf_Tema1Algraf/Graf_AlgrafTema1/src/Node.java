import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;
import java.lang.Math; 
import java.awt.Rectangle;
public class Node {
	Point m_Point;
	int number;
	Rectangle rectangle;

	public Point GetPoint()
	{
		return m_Point;
	}
	public Node(Point point,int number)
	{
		this.m_Point=point;
		this.number=number;
		rectangle = new Rectangle(point.x,point.y,30,30);
	}
	public void drawNode(Graphics graphic,int nNodeDiam)
	{
		graphic.setColor(Color.RED);
		graphic.setFont(new Font("TimesRoman",Font.BOLD,15));
		graphic.fillOval(m_Point.x,m_Point.y,nNodeDiam,nNodeDiam);
		graphic.setColor(Color.WHITE);
		graphic.drawOval(m_Point.x,m_Point.y,nNodeDiam,nNodeDiam);
		if(number<10)
			graphic.drawString(((Integer)number).toString(),m_Point.x+13,m_Point.y+20);
		else graphic.drawString(((Integer)number).toString(),m_Point.x + 8,m_Point.y + 20);
	}
	public boolean DoesNotIntersecWithNodes(Vector<Node> vNoduri)
	{
		for(Node nod:vNoduri)
			if(Math.abs(m_Point.x-nod.m_Point.x)<35.0 && Math.abs(m_Point.y-nod.m_Point.y)<35.0)
				return false;
		return true;
	}
}
