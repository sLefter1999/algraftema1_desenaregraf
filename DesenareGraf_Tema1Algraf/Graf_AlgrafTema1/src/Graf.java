import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class Graf {

	public static void initUI(String sNameUI,int nWidth,int nHeight)
	{
		JFrame frame = new JFrame(sNameUI);
		//sa se inchida aplicatia atunci cand inchid fereastra
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(new MyPanel());
        frame.setSize(nWidth,nHeight);
        frame.setVisible(true);
	}
	public static void main(String[] args) {
		initUI("Algoritmica Grafurilor",500,500);
	}
}
