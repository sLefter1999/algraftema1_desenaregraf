import java.awt.Color;
import java.awt.geom.AffineTransform;
import java.util.Vector;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Graphics;
import java.lang.Math; 
import java.awt.Rectangle;
import javax.swing.Box;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;


public class Arc {

	private Point m_ptStart;

	private Point m_ptEnd;
	
	public Node m_endNode;
	
	public Node m_startNode;

	public Arc(Point Start,Point End) 
	{
	m_ptStart = Start;
	m_ptEnd = End;
	}
	public Arc(Point Start,Point End,Node endNode) 
	{
		m_ptStart = Start;
		m_ptEnd = End;
		m_endNode=endNode;
	}
	public Point GetStartPoint()
	{
		return m_ptStart;
	}
	public Point  GetEndPoint()
	{
		return m_ptEnd;
	}
	public void SetEndNode(Node endNode)
	{
		m_endNode=endNode;
	}
	public void SetStartNode(Node startNode)
	{
		m_startNode=startNode;
	}
	public void drawArrow(Graphics g, int x1, int y1, int x2, int y2, int d, int h) 
	{
	    int dx = x2 - x1, dy = y2 - y1;
	    double D = Math.sqrt(dx*dx + dy*dy);
	    double xm = D - d, xn = xm, ym = h, yn = -h, x;
	    double sin = dy / D, cos = dx / D;

	    x = xm*cos - ym*sin + x1;
	    ym = xm*sin + ym*cos + y1;
	    xm = x;

	    x = xn*cos - yn*sin + x1;
	    yn = xn*sin + yn*cos + y1;
	    xn = x;

	    int[] xpoints = {x2, (int) xm, (int) xn};
	    int[] ypoints = {y2, (int) ym, (int) yn};

	    g.fillPolygon(xpoints, ypoints, 3);

	}
	
    private static Point LineIntersection(Point A, Point B, Point C, Point D) 
    {
        Line2D line1 = new Line2D.Double(A,B);
        Line2D line2 = new Line2D.Double(C,D);
        if(!line1.intersectsLine(line2))
        	return null;
    	
        // Line AB represented as a1x + b1y = c1 
        double a1 = B.y - A.y; 
        double b1 = A.x - B.x; 
        double c1 = a1*(A.x) + b1*(A.y); 

        // Line CD represented as a2x + b2y = c2 
        double a2 = D.y - C.y; 
        double b2 = C.x - D.x; 
        double c2 = a2*(C.x)+ b2*(C.y); 
       
        double determinant = a1*b2 - a2*b1; 
       
        if (determinant == 0) 
        { 
            // The lines are parallel. This is simplified 
            // by returning a pair of FLT_MAX 
            return null;
        } 
        else
        { 
            double x = (b2*c1 - b1*c2)/determinant; 
            double y = (a1*c2 - a2*c1)/determinant; 
            Point p = new Point();
            p.setLocation(x, y);
            return p; 
        } 
    } 
    private Point CloserToStart(Vector<Point> vPoints)
    {
    	Point p=vPoints.firstElement();
    	double d = m_ptStart.distance(vPoints.firstElement().x,vPoints.firstElement().y);
    	for(Point point:vPoints)
    		if(m_ptStart.distance(point.x,point.y)<d)
    		{
    			d=m_ptStart.distance(point.x,point.y);
    			p=point;
    		}
    	return p;
    }
	public boolean DrawArc(Graphics graphic)
	{
		if (m_ptStart != null)
		{
            graphic.setColor(Color.RED);
            graphic.drawLine(m_ptStart.x,m_ptStart.y,m_ptEnd.x,m_ptEnd.y);
            if(m_endNode!=null)
            {
            	Vector<Point> vPoints=new Vector<Point>();
            	Point p1 = new Point(m_endNode.m_Point);
            	Point p2 = new Point(p1.x+30,p1.y);
            	Point p3 = new Point(p1.x+30,p1.y+30);
            	Point p4= new Point(p1.x,p1.y+30);
            	Point intersect1 = LineIntersection(m_ptStart,m_ptEnd,p1,p2);
            	Point intersect2 = LineIntersection(m_ptStart,m_ptEnd,p2,p3);
            	Point intersect3 = LineIntersection(m_ptStart,m_ptEnd,p3,p4);
            	Point intersect4 = LineIntersection(m_ptStart,m_ptEnd,p1,p4);
            if(intersect1!=null)
            	vPoints.add(intersect1);
            if(intersect2!=null)
            	vPoints.add(intersect2);
            	if(intersect3!=null)
            vPoints.add(intersect3);
            if(intersect4!=null)
            vPoints.add(intersect4);
            	Point p5 = CloserToStart(vPoints);
            	if(p5!=null)
            	{
            		m_ptEnd=p5;
            	drawArrow(graphic,m_ptStart.x,m_ptStart.y,p5.x,p5.y,20,20);
            	}
            	
            }
            return true;
        }
		return false;
	}
	
	public Node StartNode(Vector<Node> nodes)
	{
		for(Node nod :nodes)
			if(Math.abs(nod.m_Point.x-m_ptStart.x)<30 && Math.abs(nod.m_Point.y-m_ptStart.y)<30) 
				return nod;
		
		return null;
	}
	
	public Node EndNode(Vector<Node> nodes)
	{
			for(Node nod:nodes)
			if(Math.abs(nod.m_Point.x-m_ptEnd.x)<30 && Math.abs(nod.m_Point.y-m_ptEnd.y)<30)
				return nod;
		
		return null;
	}
	
}
